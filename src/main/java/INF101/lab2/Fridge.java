package INF101.lab2;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

public class Fridge implements IFridge{
    ArrayList<FridgeItem> liste = new ArrayList<>();
    int maxsize = 20;
	/**
	 * Returns the number of items currently in the fridge
	 * 
	 * @return number of items in the fridge
	 */
    @Override
	public int nItemsInFridge() {
        int antall = 0;
        for (int i = 0; i < liste.size();){
            if (liste.get(i) != null){
                antall++;
            }
            i++;
            
        }

        return antall;
    }
    

	/**
	 * The fridge has a fixed (final) max size.
	 * Returns the total number of items there is space for in the fridge
	 * 
	 * @return total size of the fridge
	 */
    @Override
	public int totalSize() {
        return maxsize;
    }

	/**
	 * Place a food item in the fridge. Items can only be placed in the fridge if
	 * there is space
	 * 
	 * @param item to be placed
	 * @return true if the item was placed in the fridge, false if not
	 */
	@Override
	public boolean placeIn(FridgeItem item) {
        if (nItemsInFridge() < 20) {
			liste.add(item);
			return true;
		}
		else {
			return false;
		}
    }

	/**
	 * Remove item from fridge
	 * 
	 * @param item to be removed
	 * @throws NoSuchElementException if fridge does not contain <code>item</code>
	 */
	@Override
	public void takeOut(FridgeItem item){
		if(nItemsInFridge() > 0){ 
			liste.remove(item);
		}
		else {
			throw new NoSuchElementException();
		}
	}

	/**
	 * Remove all items from the fridge
	 */
	@Override
	public void emptyFridge(){
		liste.clear();
	}

	/**
	 * Remove all items that have expired from the fridge
	 * @return a list of all expired items
	 */
	@Override
	public List<FridgeItem> removeExpiredFood(){
		ArrayList<FridgeItem> expired = new ArrayList<>();
		for (int i = 0; i < nItemsInFridge(); ){
			FridgeItem temp  = liste.get(i);
			if (temp.hasExpired() == true) {
				expired.add(temp);
			}
			i++;
		}
		for(FridgeItem iterasjon : expired) {
			liste.remove(iterasjon);
		}
		System.out.print(expired);
		System.out.print(liste);
		return expired;
	}

    
    public static void main(String[] args) {

    }
}
